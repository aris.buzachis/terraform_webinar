#!/bin/bash

# wait for database to boot ;)
sleep 30

php bin/console cache:clear
php bin/console doctrine:migrations:migrate --no-interaction

chown www-data:www-data /var/www/symfony

php-fpm
