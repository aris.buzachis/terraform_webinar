<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Vote
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string")
     */
    private string $choice;

    /**
     * @ORM\Column(type="datetime")
     */
    private \DateTime $timestamp;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Vote
     */
    public function setId(int $id): Vote
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getChoice(): string
    {
        return $this->choice;
    }

    /**
     * @param string $choice
     * @return Vote
     */
    public function setChoice(string $choice): Vote
    {
        $this->choice = $choice;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTimestamp(): \DateTime
    {
        return $this->timestamp;
    }

    /**
     * @param \DateTime $timestamp
     * @return Vote
     */
    public function setTimestamp(\DateTime $timestamp): Vote
    {
        $this->timestamp = $timestamp;
        return $this;
    }
}