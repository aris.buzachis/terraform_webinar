<?php

namespace App\Command;

use App\Entity\Vote;
use App\Service\AwsS3ReportsManager;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


#[AsCommand(
    name: 'report:generate',
    description: 'This command will generate the report.',
)]
class GenerateReportCommand extends Command
{
    public function __construct(
        protected ManagerRegistry $doctrine,
        protected AwsS3ReportsManager $reportsManager,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Generating the report...');

        $em = $this->doctrine->getManager();
        $votes = $em->getRepository(Vote::class)->findAll();
        if (count($votes) === 0) {
            $output->writeln('No votes found. Skipping.');
        } else {
            $csvReport = $this->generateReport($votes);

            $filename = sprintf('report-%s.csv', (new \DateTime())->format(\DateTimeInterface::ATOM));
            $this->reportsManager->uploadReport($filename, $csvReport);

            $output->writeln('Report generated and uploaded.');
        }

        return Command::SUCCESS;
    }

    private function generateReport(array $votes): string
    {
        $content = array_map(function (Vote $vote) {
            return $this->array2Csv([ $vote->getChoice(), $vote->getTimestamp()->format(\DateTimeInterface::ATOM) ]);
        }, $votes);
        array_unshift($content, $this->array2Csv(['Choice', 'Vote']));

        return implode("", $content);
    }

    private function array2Csv(array $fields, string $delimiter = ",", $enclosure = '"', $escapeChar = "\\"): string
    {
        $buffer = fopen('php://temp', 'r+');
        fputcsv($buffer, $fields, $delimiter, $enclosure, $escapeChar);
        rewind($buffer);
        $csv = fgets($buffer);
        fclose($buffer);
        return $csv;
    }
}
