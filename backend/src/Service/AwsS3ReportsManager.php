<?php

namespace App\Service;

use Aws\S3\S3Client;

class AwsS3ReportsManager
{
    protected S3Client $client;

    public function __construct(
        protected string $reportsAwsAccessKeyId,
        protected string $reportsAwsSecretAccessKey,
        protected string $reportsAwsRegion,
        protected string $reportsBucketName
    )
    {
        $this->setClient(new S3Client([
            'credentials' => [
                'key' => $this->reportsAwsAccessKeyId,
                'secret' => $this->reportsAwsSecretAccessKey,
            ],
            'version' => '2006-03-01',
            'region' => $this->reportsAwsRegion
        ]));
    }

    public function uploadReport(string $key, string $content): string
    {
        return $this->getClient()->upload($this->reportsBucketName, $key, $content)->toArray()['ObjectURL'];
    }

    protected function getClient(): S3Client
    {
        return $this->client;
    }

    protected function setClient(S3Client $client): void
    {
        $this->client = $client;
    }
}
