<?php

namespace App\Controller;

use App\Entity\Vote;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api", name="api_")
 */
class VotesController extends AbstractController
{
    /**
     * @Route("/choices", name="coices_list", methods={"GET"})
     */
    public function listChoicesAction(string $voteChoices): Response
    {
        $choices = $this->getAvailableChoices($voteChoices);

        return $this->json(['data' => $choices]);
    }

    /**
     * @Route("/vote", name="vote_new", methods={"POST"})
     */
    public function new(Request $request, ManagerRegistry $doctrine, string $voteChoices): Response
    {
        $parametersAsArray = [];
        if ($content = $request->getContent()) {
            $parametersAsArray = json_decode($content, true);
        }

        $choiceValue = $parametersAsArray['choice'] ?? null;
        if (empty($choiceValue)) {
            return $this->json(['message' => 'A choice value is mandatory.'], Response::HTTP_BAD_REQUEST);
        }

        $validChoices = $this->getAvailableChoices($voteChoices);
        if (!in_array($choiceValue, $validChoices, true)) {
            return $this->json(['message' => 'Invalid choice value provided.'], Response::HTTP_BAD_REQUEST);
        }

        $entityManager = $doctrine->getManager();

        $vote = new Vote();
        $vote
            ->setChoice($choiceValue)
            ->setTimestamp(new \DateTime())
        ;

        $entityManager->persist($vote);
        $entityManager->flush($vote);

        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    private function getAvailableChoices(string $voteChoicesStr): array
    {
        return array_filter(explode(',', $voteChoicesStr));
    }
}
