provider "aws" {
  region = var.aws_region
}

provider "cloudflare" {}

locals {
  cloudflare_subdomain = "tfdemo-${var.prefix}"
  api_endpoint_url = replace(var.api_endpoint_url_template, "%prefix%", var.prefix)
}

# prepare the website public bucket
resource "aws_s3_bucket" "site" {
  bucket = "${local.cloudflare_subdomain}.${var.cloudflare_domain}"

  force_destroy = true

  website {
    index_document = "index.html"
    error_document = "error.html"
  }
}

resource "aws_s3_bucket_acl" "site" {
  bucket = aws_s3_bucket.site.id

  acl = "public-read"
}

resource "aws_s3_bucket_policy" "site" {
  bucket = aws_s3_bucket.site.id

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Sid       = "PublicReadGetObject"
        Effect    = "Allow"
        Principal = "*"
        Action    = "s3:GetObject"
        Resource = [
          aws_s3_bucket.site.arn,
          "${aws_s3_bucket.site.arn}/*",
        ]
      },
    ]
  })
}


# prepare the cloudflare zone records
data "cloudflare_zones" "domain" {
  filter {
    name = var.cloudflare_domain
  }
}

data "aws_s3_bucket" "site_bucket" {
  bucket = aws_s3_bucket.site.bucket
}

resource "cloudflare_record" "site_cname" {
  zone_id = data.cloudflare_zones.domain.zones[0].id
  name    = local.cloudflare_subdomain
  value   = data.aws_s3_bucket.site_bucket.website_endpoint
  type    = "CNAME"

  ttl     = 1
  proxied = true
}

# upload the website files
# first get the list and compute the content types
module "website_files" {
  source = "hashicorp/dir/template"

  base_dir = "${path.module}/website"
}

resource "aws_s3_bucket_object" "file_upload" {
  for_each = module.website_files.files

  bucket  = aws_s3_bucket_policy.site.bucket
  key     = each.key
  content_type = each.value.content_type

  source = each.value.source_path
  content = each.value.content
}

# handle the index.html file
resource "aws_s3_bucket_object" "index_html_upload" {
  bucket  = aws_s3_bucket_policy.site.bucket
  key     = "index.html"
  content_type = "text/html; charset=utf-8"

  content = replace(file("${path.module}/website/index.html"), "http://localhost:8080/api", local.api_endpoint_url)

  etag = md5(replace(file("${path.module}/website/index.html"), "http://localhost:8080/api", local.api_endpoint_url))

  depends_on = [aws_s3_bucket_object.file_upload]
}

# prepare the reports storage bucket and user
resource "aws_iam_user" "reports_user" {
  name = "${var.prefix}_reports"
}

resource "aws_iam_access_key" "reports_user" {
  user = aws_iam_user.reports_user.name
}

resource "aws_s3_bucket" "reports_bucket" {
  bucket = "${var.prefix}-tfdemo-reports"

  force_destroy = true
}

resource "aws_iam_user_policy" "user_policy" {
  name = "ReportsAccess_${var.prefix}"
  user = aws_iam_user.reports_user.name

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        "Effect" : "Allow",
        "Action" : "s3:*",
        "Resource" : [
          aws_s3_bucket.reports_bucket.arn,
          "${aws_s3_bucket.reports_bucket.arn}/*"
        ]
      }
    ]
  })
}
