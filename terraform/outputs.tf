output "website_url" {
  value = "http://${local.cloudflare_subdomain}.${var.cloudflare_domain}"
}

output "reports_iam_user_access_key_id" {
  value = aws_iam_access_key.reports_user.id
}

output "reports_iam_user_secret_access_key" {
  value = aws_iam_access_key.reports_user.secret
  sensitive = true
}

output "reports_bucket_name" {
  value = aws_s3_bucket.reports_bucket.bucket
}
