variable "aws_region" {
  type        = string
  description = "The AWS region to put the bucket into"
  default     = "eu-west-1"
}

variable "cloudflare_domain" {
  type        = string
  description = "The Cloudflare domain to use."
}

variable "prefix" {
  type        = string
  description = "The prefix used for all the resources created by the module."
}

variable "api_endpoint_url_template" {
  type        = string
  description = "The API endpoint URL."
}
