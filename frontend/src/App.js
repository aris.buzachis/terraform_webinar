import React, { useState, useEffect }  from 'react';

import './App.scss';

const App = () => {
    const [error, setError] = useState(null);
    const [hasVoted, setHasVoted] = useState(false);
    const [isLoaded, setIsLoaded] = useState(false);
    const [choices, setChoices] = useState([]);

    useEffect(() => {
        fetch(window.apiEndpointUrl + "/choices")
            .then(res => res.json())
            .then(
                (response) => {
                    setIsLoaded(true);
                    setChoices(response.data);
                },
                (error) => {
                    setIsLoaded(true);
                    setError(error);
                }
            );
    }, []);

    const sendVote = (choice) => {
        fetch(window.apiEndpointUrl + "/vote", {
            method: 'POST',
            body: JSON.stringify({ choice }),
        })
            .then(
                () => {
                    setHasVoted(true);
                },
                (error) => {
                    setIsLoaded(true);
                    setError(error);
                }
            );
    }

    if (error) {
        return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
        return <div>Loading...</div>;
    } else if (hasVoted) {
        return <div>Thank you for your vote!</div>;
    } else {
        return (
            <div className="choices-container">
                {choices.map(choice => (
                    <button key={choice} onClick={() => sendVote(choice)} style={{color: choice}}>
                        {choice}
                    </button>
                ))}
            </div>
        );
    }
};

export default App;
